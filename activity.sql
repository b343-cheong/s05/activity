SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

SELECT quantityOrdered FROM orderdetails JOIN orders ON orderdetails.orderNumber = orders.orderNumber WHERE comments LIKE "%DHL%";

SELECT textDescription FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status from orders;

SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

SELECT firstName, lastName, city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";

SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

SELECT productName, customerName FROM products 
JOIN orderdetails ON products.productCode = orderdetails.productCode 
JOIN orders ON orderdetails.orderNumber = orders.orderNumber 
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customerName = "Baane Mini Imports";

SELECT firstName, lastName, customerName, offices.country FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE "%+81%";
